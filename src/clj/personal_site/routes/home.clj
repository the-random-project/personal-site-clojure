(ns personal-site.routes.home
  (:require
   [personal-site.layout :as layout]
   [personal-site.db.core :as db]
   [clojure.java.io :as io]
   [personal-site.middleware :as middleware]
   [ring.util.response]
   [ring.util.http-response :as response]))



(defn home-page [request]
  (layout/render request "index.html"))

(defn projects-page [request]
  (layout/render request "projects.html"))

(defn quals-page [request]
  (layout/render request "qualifications.html"))

(defn home-routes []
  [""
   {:middleware [middleware/wrap-csrf
                 middleware/wrap-formats]}
   ["/" {:get home-page}]
   ["/projects" {:get projects-page}]
   ["/qualifications" {:get quals-page}]])


FROM openjdk:8-alpine

COPY target/uberjar/personal-site.jar /personal-site/app.jar

EXPOSE 3000

CMD ["java", "-jar", "/personal-site/app.jar"]

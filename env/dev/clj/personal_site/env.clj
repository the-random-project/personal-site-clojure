(ns personal-site.env
  (:require
    [selmer.parser :as parser]
    [clojure.tools.logging :as log]
    [personal-site.dev-middleware :refer [wrap-dev]]))

(def defaults
  {:init
   (fn []
     (parser/cache-off!)
     (log/info "\n-=[personal-site started successfully using the development profile]=-"))
   :stop
   (fn []
     (log/info "\n-=[personal-site has shut down successfully]=-"))
   :middleware wrap-dev})

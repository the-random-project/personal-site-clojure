(ns personal-site.env
  (:require [clojure.tools.logging :as log]))

(def defaults
  {:init
   (fn []
     (log/info "\n-=[personal-site started successfully]=-"))
   :stop
   (fn []
     (log/info "\n-=[personal-site has shut down successfully]=-"))
   :middleware identity})

# Personal Site

This is my personal site redesigned around using Clojure and Luminous with blog capabilities.

## Prerequisites

You will need:
- [Leiningen][1] 2.0 or above installed.
- [PostgreSQL][2] installed
- [Clojure][3] Installed

[1]: https://codeberg.org/leiningen/leiningen
[2]: https://www.postgresql.org/
[3]: https://clojure.org/

## Running

To start a web server for the application, run:

    lein run 

## License

Copyright © 2022 GPLv3
